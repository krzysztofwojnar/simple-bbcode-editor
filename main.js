const textarea = document.getElementsByTagName('textarea')[0];
const preview = document.getElementById('result');
const styles = Object.freeze({
    AMPERSAND: '&',
    SPACE: ' ',
    NEWLINE: '\n',
    ITALIC: {start: '[i]', end: '[/i]'},
    BOLD: {start: '[b]', end: '[/b]'},
    LIST: {start: '[list]', end: '[/list]'},
    LISTITEM: '[*]'
});
const refreshView = function () {
    const rules = [
        [styles.AMPERSAND, '&amp;'],
        [styles.SPACE, '\xa0'],
        [styles.NEWLINE, '<br>'],
        [styles.ITALIC.start, '<em>'],
        [styles.ITALIC.end, '</em>'],
        [styles.BOLD.start, '<b>'],
        [styles.BOLD.end, '</b>'],
        [styles.LIST.start, '<ul>'],
        [styles.LIST.end, '</ul>'],
        [styles.LISTITEM, '<li>']
    ];
    preview.innerHTML = rules.reduce(
        (acc, rule) => acc.replaceAll(rule[0], rule[1]),
        textarea.value
    );
};
textarea.addEventListener('keyup', refreshView);

function formatText(button) {
    const markups = styles[button.id.toUpperCase()]
    const oldSelection = {start: textarea.selectionStart, end: textarea.selectionEnd};
    if (typeof markups === 'object') {
        if (
            (textarea.value.substring(textarea.selectionStart - markups.start.length, textarea.selectionStart) ===
                markups.start) &&
            (textarea.value.substring(textarea.selectionEnd + markups.end.length, textarea.selectionEnd) ===
                markups.end)
        ) {
            textarea.value =
                textarea.value.substring(0, textarea.selectionStart - markups.start.length)
                + textarea.value.substring(textarea.selectionStart, textarea.selectionEnd) +
                textarea.value.substring(textarea.selectionEnd + markups.end.length);
            textarea.selectionStart = oldSelection.start - markups.start.length;
            textarea.selectionEnd = oldSelection.end - markups.start.length;
        } else {
            const selectionLength = textarea.selectionEnd - textarea.selectionStart;
            const selectionEnd = oldSelection.start + markups.start.length + selectionLength;
            textarea.value =
                textarea.value.substring(0, textarea.selectionStart)
                + markups.start
                + textarea.value.substring(textarea.selectionStart);
            textarea.value =
                textarea.value.substring(0, selectionEnd)
                + markups.end
                + textarea.value.substring(selectionEnd);
            textarea.selectionStart = oldSelection.start + markups.start.length
            textarea.selectionEnd = oldSelection.start + markups.start.length + selectionLength;
        }
    } else {
        if (textarea.value.substring(textarea.selectionStart - markups.length, textarea.selectionStart) === markups) {
            textarea.value =
                textarea.value.substring(0, textarea.selectionStart - markups.length)
                + textarea.value.substring(textarea.selectionStart);
            textarea.selectionStart = oldSelection.start - markups.length;
            textarea.selectionEnd = oldSelection.end - markups.length;
        } else {
            textarea.value =
                textarea.value.substring(0, textarea.selectionStart)
                + markups
                + textarea.value.substring(textarea.selectionStart);
            textarea.selectionStart = oldSelection.start + markups.length;
            textarea.selectionEnd = oldSelection.end + markups.length;
        }
    }
    textarea.focus();
    refreshView();
}

const save = () => {
    const a = document.createElement('a');
    const file = new Blob([JSON.stringify(
        {
            data: textarea.value,
            textareaSize: {height: textarea.style.height, width: textarea.style.width}
        }
    )], {type: 'application/json'});
    const fileName = document.getElementById('file-name').value
    a.download = (fileName ? fileName : 'noname') + '.json'
    a.href = URL.createObjectURL(file);
    a.click();
    URL.revokeObjectURL(a.href);
    console.log(JSON.stringify(textarea.value).length);
}
const openFile = () => {
    const inputElement = document.createElement('input');
    inputElement.setAttribute('type', 'file');
    inputElement.setAttribute('accept', 'application/json');
    inputElement.click();
    inputElement.addEventListener('change', async () => {
        const jsonObject = JSON.parse(await inputElement.files[0].text());
        if (!jsonObject.textareaSize?.width || !jsonObject.textareaSize?.height) {
            alert('This JSON file is incompatible.');
            return;
        }
        textarea.value = jsonObject.data;
        textarea.style.height = jsonObject.textareaSize.height;
        textarea.style.width = jsonObject.textareaSize.width;
        document.getElementById('file-name').value = inputElement.files[0].name.slice(0, -5);
        refreshView();
        textarea.focus();
    })
}
textarea.focus();
